﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	private Animator animator;
	public GameObject portero;
	void Start () {
		animator = transform.GetChild(0).GetComponent<Animator>();
	}
	
	void Update () {
		if (Input.GetKeyDown(KeyCode.A)) {
			animator.SetTrigger("kick");
			portero.SendMessage("Catch");
			Debug.Log("a");
			Invoke("ChangeState", 5.15f);
		}
	}

	void ChangeState() {
		animator.applyRootMotion = false;
		animator.applyRootMotion = true;
	}
}
