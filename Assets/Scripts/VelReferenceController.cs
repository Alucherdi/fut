﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelReferenceController : MonoBehaviour {
	public float min;
	public float max;
	public float vel;
	private float xvel;
	void Start () {
		xvel = -vel;
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Rigidbody>().velocity = new Vector3(xvel, 0, 0);
		if (transform.position.x < min) {
			xvel = vel;
		}
		
		if (transform.position.x > max) {
			xvel = -vel;
		}
	}
}
