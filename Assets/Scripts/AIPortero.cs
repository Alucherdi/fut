﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPortero : MonoBehaviour {
	public Transform posReference;
	private Animator anim;
	public bool isCatching = false;

	void Start() {
		anim = transform.GetChild(0).gameObject.GetComponent<Animator>();
	}

	void Update() {

	}

	void Catch() {
		anim.SetTrigger("catch");
		anim.applyRootMotion = true;
		isCatching = true;
		if (transform.position.x <= posReference.position.x) {
			transform.localScale = new Vector3(-1, 1, 1);
		}

		if (transform.position.x > posReference.position.x) {
			transform.localScale = new Vector3(1, 1, 1);
		}
	}
}


