﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PorteroController : MonoBehaviour {
	public float vel, min, max;
	private Rigidbody rb;
	private float velx;

	private AIPortero ai;

	void Start() {
		rb = GetComponent<Rigidbody>();
		ai = GetComponent<AIPortero>();
		transform.localScale = new Vector3(-1, 1, 1);
		velx = vel;
	}

	void Update() {
		if (!ai.isCatching) {
			rb.velocity = new Vector3(velx, 0, 0);
			if (transform.position.x < min) {
				velx = vel;
				transform.localScale = new Vector3(-1, 1, 1);
			}

			if (transform.position.x > max) {
				velx = -vel;
				transform.localScale = new Vector3(1, 1, 1);
			}
		} else {
			Vector3 momentum = rb.velocity;
			rb.velocity = Vector3.zero;
		}
	}
}
