﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {
	public GameObject velRef;
	Rigidbody rb;
	bool hitted = false;
	void Start () {
		rb = GetComponent<Rigidbody>();
	}

	void Reset() {
		transform.position = new Vector3(0, -0.522f, 5.514143f);
		rb.velocity = Vector3.zero;
		rb.angularVelocity = Vector3.zero;
		hitted = false;
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "foot" && !hitted) {
			transform.LookAt(velRef.transform);
			rb.AddRelativeForce(new Vector3(0, 0, 80), ForceMode.Impulse);
			hitted = true;
		}

		if (other.gameObject.tag == "porteria") {
			//Reset();
			other.gameObject.GetComponent<ScoreController>().score ++;
		}
	}
}
